<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Unit;

use LaraproophTests\ServiceBus\Mock\AMessageBusManager;
use PHPUnit\Framework\TestCase;
use Prooph\ServiceBus\MessageBus;

class ServiceBusManagerTest extends TestCase
{
    private $bus;

    public function setUp(): void
    {
        $this->bus = $this->getMockForAbstractClass(MessageBus::class);
    }

    /**
     * @test
     */
    public function it_return_default_service_bus_instance_when_no_bus_provided(): void
    {
        $m = $this->getMockManager();
        $m->expects($this->once())->method('getDefaultServiceBus')->willReturn('foo');
        $m->expects($this->once())->method('getRoutesForService')->willReturn([]);
        $m->expects($this->once())->method('resolve')->willReturn($this->bus);

        $this->assertInstanceOf(MessageBus::class, $m->create());
    }

    /**
     * @test
     */
    public function it_return_same_bus_instance(): void
    {
        $m = $this->getMockManager();
        $m->expects($this->never())->method('getDefaultServiceBus');
        $m->expects($this->once())->method('getRoutesForService')->willReturn([]);
        $m->expects($this->once())->method('resolve')->willReturn($this->bus);

        $this->assertInstanceOf(MessageBus::class, $m->create('foo'));
        $this->assertSame($this->bus, $m->create('foo'));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function it_raise_exception_when_no_bus_configured(): void
    {
        $m = $this->getMockManager();
        $m->method('getDefaultServiceBus')->willReturn(null);
        $m->create();
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|AMessageBusManager
     */
    public function getMockManager()
    {
        return $this->getMockBuilder(AMessageBusManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['resolve', 'getServiceBus', 'getDefaultServiceBus', 'getRoutesForService', 'getOptionsFor'])
            ->getMock();
    }
}