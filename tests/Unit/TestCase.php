<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Unit;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\Container as ContainerContract;
use Illuminate\Foundation\Application;
use PHPUnit\Framework\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    /**
     * @var ContainerContract
     */
    protected $app;

    public function setUp(): void
    {
        $this->app = new Application();
        $this->app->bind(ContainerContract::class, Container::class);
    }
}