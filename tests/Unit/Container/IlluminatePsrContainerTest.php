<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Unit\Container;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\Container as ContainerContract;
use Laraprooph\ServiceBus\Container\IlluminatePsrContainer;
use LaraproophTests\ServiceBus\Mock\SomeClass;
use PHPUnit\Framework\TestCase;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Plugin\Router\CommandRouter;

class IlluminatePsrContainerTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_be_constructed_with_illuminate_container(): void
    {
        $m = $this->getMockForAbstractClass(ContainerContract::class);
        $ins = new IlluminatePsrContainer($m);

        $this->assertInstanceOf(IlluminatePsrContainer::class, $ins);
    }

    /**
     * @test
     */
    public function it_can_check_if_service_id_exists(): void
    {
        $m = $this->getMockForAbstractClass(ContainerContract::class);
        $m->expects($this->once())->method('bound')->willReturn(true);
        $ins = new IlluminatePsrContainer($m);

        $this->assertTrue($ins->has('foo'));
    }

    /**
     * @test
     */
    public function it_can_check_if_service_class_exists(): void
    {
        $app = $this->getContainer();
        $app->bind(SomeClass::class);

        $ins = new IlluminatePsrContainer($app);

        $this->assertTrue($ins->has(SomeClass::class));
    }

    /**
     * @test
     */
    public function it_can_resolve_service(): void
    {
        $m = $this->getMockForAbstractClass(ContainerContract::class);
        $m->expects($this->once())->method('bound')->willReturn(true);
        $m->expects($this->once())->method('make')->willReturn('foo');
        $ins = new IlluminatePsrContainer($m);

        $this->assertEquals('foo', $ins->get('foo'));
    }

    public function it_attach_router_to_message_bus(): void
    {
        $config = [
            'concrete' => CommandBus::class,
            'router' => [
                'concrete' => CommandRouter::class
            ]
        ];

        $options = [];
    }

    /**
     * @test
     * @expectedException \Laraprooph\ServiceBus\Container\Exception\ContainerAliasNotFoundException
     */
    public function it_raise_exception_when_service_does_not_exists(): void
    {
        $m = $this->getMockForAbstractClass(ContainerContract::class);
        $m->expects($this->once())->method('bound')->willReturn(false);
        $ins = new IlluminatePsrContainer($m);

        $ins->get('bar');
    }

    public function getContainer(): ContainerContract
    {
        $app = new Container();
        $app->bind(ContainerContract::class, Container::class);

        return $app;
    }
}