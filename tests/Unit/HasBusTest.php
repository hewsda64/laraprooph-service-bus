<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Unit;

use Laraprooph\ServiceBus\CommandBusManager;
use Laraprooph\ServiceBus\EventBusManager;
use Laraprooph\ServiceBus\QueryBusManager;
use LaraproophTests\ServiceBus\Mock\SomeClassWithHasBus;
use Prooph\Common\Messaging\Message;
use Prooph\ServiceBus\MessageBus;

class HasBusTest extends TestCase
{
    private $message;

    public function setUp(): void
    {
        parent::setUp();

        $this->message = $this->getMockForAbstractClass(Message::class);
    }

    /**
     * @test
     */
    public function it_dispatch_command_with_default_bus(): void
    {
        $m = $this->getMockBuilder(CommandBusManager::class)->disableOriginalConstructor()->getMock();
        $bus = $this->getMockForAbstractClass(MessageBus::class);

        $m->expects($this->once())->method('command')->willReturn($bus);
        $bus->expects($this->once())->method('dispatch');

        $this->app->bind(CommandBusManager::class, function () use ($m) {
            return $m;
        });

        $aClass = new SomeClassWithHasBus();
        $aClass->dispatchSomeCommand($this->message);
    }

    /**
     * @test
     */
    public function it_dispatch_command_with_configured_bus(): void
    {
        $m = $this->getMockBuilder(CommandBusManager::class)->disableOriginalConstructor()->getMock();
        $bus = $this->getMockForAbstractClass(MessageBus::class);

        $m->expects($this->never())->method('command');
        $bus->expects($this->once())->method('dispatch');

        $this->app->bind(CommandBusManager::class, function () use ($m) {
            return $m;
        });

        $this->app->bind('foobar', function () use ($bus) {
            return $bus;
        });

        $aClass = new SomeClassWithHasBus();
        $aClass->dispatchSomeCommand($this->message, 'foobar');
    }

    /**
     * @test
     */
    public function it_dispatch_event_with_default_bus(): void
    {
        $m = $this->getMockBuilder(EventBusManager::class)->disableOriginalConstructor()->getMock();
        $bus = $this->getMockForAbstractClass(MessageBus::class);

        $m->expects($this->once())->method('event')->willReturn($bus);
        $bus->expects($this->once())->method('dispatch');

        $this->app->bind(EventBusManager::class, function () use ($m) {
            return $m;
        });

        $aClass = new SomeClassWithHasBus();
        $aClass->dispatchSomeEvent($this->message);
    }

    /**
     * @test
     */
    public function it_dispatch_event_with_configured_bus(): void
    {
        $m = $this->getMockBuilder(EventBusManager::class)->disableOriginalConstructor()->getMock();
        $bus = $this->getMockForAbstractClass(MessageBus::class);

        $m->expects($this->never())->method('event');
        $bus->expects($this->once())->method('dispatch');

        $this->app->bind(EventBusManager::class, function () use ($m) {
            return $m;
        });

        $this->app->bind('foobar', function () use ($bus) {
            return $bus;
        });

        $aClass = new SomeClassWithHasBus();
        $aClass->dispatchSomeEvent($this->message, 'foobar');
    }

    /**
     * @test
     */
    public function it_dispatch_query_with_default_bus(): void
    {
        $m = $this->getMockBuilder(QueryBusManager::class)->disableOriginalConstructor()->getMock();
        $bus = $this->getMockForAbstractClass(MessageBus::class);

        $m->expects($this->once())->method('query')->willReturn($bus);
        $bus->expects($this->once())->method('dispatch');

        $this->app->bind(QueryBusManager::class, function () use ($m) {
            return $m;
        });

        $aClass = new SomeClassWithHasBus();
        $aClass->dispatchSomeQuery($this->message);
    }

    /**
     * @test
     */
    public function it_dispatch_query_with_configured_bus(): void
    {
        $m = $this->getMockBuilder(QueryBusManager::class)->disableOriginalConstructor()->getMock();
        $bus = $this->getMockForAbstractClass(MessageBus::class);

        $m->expects($this->never())->method('query');
        $bus->expects($this->once())->method('dispatch');

        $this->app->bind(QueryBusManager::class, function () use ($m) {
            return $m;
        });

        $this->app->bind('foobar', function () use ($bus) {
            return $bus;
        });

        $aClass = new SomeClassWithHasBus();
        $aClass->dispatchSomeQuery($this->message, 'foobar');
    }
}