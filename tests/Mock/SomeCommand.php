<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Mock;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

class SomeCommand extends Command implements PayloadConstructable
{
    use PayloadTrait;
}