<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Mock;

use Laraprooph\ServiceBus\HasBus;

class SomeClassWithHasBus
{
    use HasBus;

    public function dispatchSomeCommand($command, string $bus = null): void
    {
        $this->dispatchCommand($command, $bus);
    }

    public function dispatchSomeEvent($event, string $bus = null): void
    {
        $this->dispatchEvent($event, $bus);
    }

    public function dispatchSomeQuery($command, string $bus = null)
    {
        return $this->dispatchQuery($command, $bus);
    }
}