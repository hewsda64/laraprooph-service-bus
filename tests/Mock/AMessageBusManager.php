<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Mock;

use Laraprooph\ServiceBus\ServiceBusManager;

class AMessageBusManager extends ServiceBusManager
{
    public function type(): string
    {
        return 'foo';
    }
}