<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus;

use Prooph\ServiceBus\MessageBus;

class CommandBusManager extends ServiceBusManager
{
    public function command(string $commandBusAlias = null, array $routes = []): MessageBus
    {
        return parent::create($commandBusAlias, $routes);
    }

    public function type(): string
    {
        return 'command';
    }
}