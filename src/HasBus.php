<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus;

trait HasBus
{

    protected function dispatchCommand($command, string $busName = null): void
    {
        if ($busName) {
            app($busName)->dispatch($command);
            return;
        }

        app(CommandBusManager::class)->command()->dispatch($command);
    }

    protected function dispatchQuery($query, string $busName = null)
    {
        if ($busName) {
            return app($busName)->dispatch($query);
        }

        return app(QueryBusManager::class)->query($busName)->dispatch($query);
    }

    protected function dispatchEvent($event, string $busName = null): void
    {
        if ($busName) {
            app($busName)->dispatch($event);
            return;
        }

        app(EventBusManager::class)->event($busName)->dispatch($event);
    }
}