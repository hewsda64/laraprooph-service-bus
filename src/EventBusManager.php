<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus;

use Prooph\ServiceBus\MessageBus;

class EventBusManager extends ServiceBusManager
{
    public function event(string $eventBusAlias = null, array $routes = []): MessageBus
    {
        return parent::create($eventBusAlias, $routes);
    }

    public function type(): string
    {
        return 'event';
    }
}