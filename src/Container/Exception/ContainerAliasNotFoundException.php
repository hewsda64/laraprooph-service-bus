<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus\Container\Exception;

use Psr\Container\NotFoundExceptionInterface;

class ContainerAliasNotFoundException extends \RuntimeException implements NotFoundExceptionInterface
{
}