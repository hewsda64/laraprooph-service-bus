<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus\Container;

use Illuminate\Contracts\Container\Container as IlluminateContainer;
use Laraprooph\ServiceBus\Container\Exception\ContainerAliasNotFoundException;
use Psr\Container\ContainerInterface;

class IlluminatePsrContainer implements ContainerInterface
{
    /**
     * @var IlluminateContainer
     */
    private $container;

    /**
     * IlluminatePsrContainer constructor.
     *
     * @param IlluminateContainer $container
     */
    public function __construct(IlluminateContainer $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $id
     *
     * @return mixed
     * @throws \Laraprooph\ServiceBus\Container\Exception\ContainerAliasNotFoundException
     */
    public function get($id)
    {
        if ($this->has($id)) {
            return $this->container->make($id);
        }

        $id = is_string($id) ? $id : gettype($id);

        throw new ContainerAliasNotFoundException("Container alias not found with service {$id}");
    }

    public function has($id): bool
    {
        if ($this->container->bound($id)) {
            return true;
        }

        return class_exists($id);
    }
}