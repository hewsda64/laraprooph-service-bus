<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus;

use Illuminate\Contracts\Foundation\Application;
use Laraprooph\ServiceBus\Container\IlluminatePsrContainer;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\Plugin\Router\AsyncSwitchMessageRouter;
use Prooph\ServiceBus\Plugin\ServiceLocatorPlugin;
use Psr\Container\ContainerInterface;

abstract class ServiceBusManager
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var array
     */
    protected $messageBus = [];

    /**
     * @var string
     */
    protected $namespace = 'laraprooph';

    /**
     * ServiceBusManager constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function create(string $serviceBus = null, array $routes = []): MessageBus
    {
        $busName = $serviceBus ?? $this->getDefaultServiceBus();

        if (!$busName) {
            throw new \InvalidArgumentException("Service bus not found for {$this->type()}.");
        }

        if (isset($this->messageBus[$busName])) {
            return $this->messageBus[$busName];
        }

        $routes = !empty($routes) ? $routes : $this->getRoutesForService($busName);

        return $this->messageBus[$busName] = $this->resolve($busName, $routes);
    }

    protected function resolve(string $busName, array $routes): MessageBus
    {
        $config = $this->getServiceBus($busName);

        if (empty($config)) {
            throw new \InvalidArgumentException(
                "Service bus {$busName} not found in laraprooph configuration."
            );
        }

        return $this->createBus($config, $routes);
    }

    protected function createBus(array $config, array $routes): MessageBus
    {
        $serviceBus = new $config['concrete'];
        $options = $this->getOptionsFor($config);
        $plugins = $options['plugins'];

        if (true === $options['enable_handler_location']) {
            $plugins[] = $this->registerServiceLocator();
        }

        $this->registerBusPlugins($serviceBus, $plugins);

        $this->attachRouterToBus($serviceBus, $config['router'], $routes);

        return $serviceBus;
    }

    protected function attachRouterToBus(MessageBus $bus, array $routerConfig, array $routes): void
    {
        $router = new $routerConfig['concrete']($routes);

        if (isset($routerConfig['async_switch'])) {
            $asyncMessageProducer = $this->app->make($routerConfig['async_switch']);

            $router = new AsyncSwitchMessageRouter($router, $asyncMessageProducer);
        }

        $router->attachToMessageBus($bus);
    }

    protected function registerServiceLocator(): string
    {
        $this->app->bind(ContainerInterface::class, IlluminatePsrContainer::class);

        $this->app->bind(ServiceLocatorPlugin::class);

        return ServiceLocatorPlugin::class;
    }

    protected function registerBusPlugins(MessageBus $bus, array $plugins): void
    {
        foreach ($plugins as $plugin) {
            $this->app->bindIf($plugin);

            $this->app[$plugin]->attachToMessageBus($bus);
        }
    }

    protected function getServiceBus(string $busName): array
    {
        return $this->fromConfig('bus_manager.service_bus.' . $this->type() . '.' . $busName, []);
    }

    protected function getDefaultServiceBus(): ?string
    {
        return $this->fromConfig('bus_manager.defaults.' . $this->type());
    }

    protected function getRoutesForService(string $busName): array
    {
        return $this->fromConfig('bus_router.' . $busName . '.router.routes', []);
    }

    protected function getOptionsFor(array $config): array
    {
        $default = $this->fromConfig('bus_manager.options', []);

        return array_merge($default, $config['options'] ?? []);
    }

    protected function fromConfig(string $key, $default = null)
    {
        return $this->app['config']->get('service_bus.' . $this->namespace . '.' . $key, $default);
    }

    abstract public function type(): string;
}