<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus;

use Prooph\ServiceBus\MessageBus;

class QueryBusManager extends ServiceBusManager
{
    public function query(string $queryBusAlias = null, array $routes = []): MessageBus
    {
        return parent::create($queryBusAlias, $routes);
    }

    public function type(): string
    {
        return 'query';
    }
}