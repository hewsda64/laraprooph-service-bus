<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus\Provider;

use Illuminate\Support\ServiceProvider;
use Laraprooph\ServiceBus\CommandBusManager;
use Laraprooph\ServiceBus\EventBusManager;
use Laraprooph\ServiceBus\QueryBusManager;

class ServiceBusServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    public function boot(): void
    {
        $this->publishes([
            __DIR__ . '/../../config/service_bus.php' => config_path('service_bus.php')
        ], 'laraprooph');
    }

    public function register(): void
    {
        $this->registerConfig();

        $this->registerBusManager();
    }

    protected function registerConfig(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/service_bus.php', 'laraprooph');
    }

    protected function registerBusManager(): void
    {
        $this->app->singleton(CommandBusManager::class);

        $this->app->singleton(EventBusManager::class);

        $this->app->singleton(QueryBusManager::class);
    }

    public function provides(): array
    {
        return [
            CommandBusManager::class,
            EventBusManager::class,
            QueryBusManager::class,
        ];
    }
}