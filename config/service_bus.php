<?php

return [
    'laraprooph' => [

        'bus_manager' => [

            'defaults' => [
                'command' => 'command_bus',
                'event' => 'event_bus',
                'query' => 'query_bus'
            ],

            'service_bus' => [

                'command' => [
                    'command_bus' => [
                        'concrete' => \Prooph\ServiceBus\CommandBus::class,
                        'router' => [
                            'concrete' => \Prooph\ServiceBus\Plugin\Router\CommandRouter::class,
                        ]
                    ],
                ],

                'event' => [
                    'event_bus' => [
                        'concrete' => \Prooph\ServiceBus\EventBus::class,
                        'router' => [
                            'concrete' => \Prooph\ServiceBus\Plugin\Router\EventRouter::class,
                        ],
                        'options' => [
                            'plugins' => [
                                \Prooph\ServiceBus\Plugin\InvokeStrategy\OnEventStrategy::class,
                            ],
                        ]
                    ]
                ],

                'query' => [
                    'query_bus' => [
                        'concrete' => \Prooph\ServiceBus\QueryBus::class,
                        'router' => [
                            'concrete' => \Prooph\ServiceBus\Plugin\Router\QueryRouter::class,
                        ],
                    ]
                ]
            ],


            'options' => [
                'enable_handler_location' => true,
                'message_factory' => \Prooph\Common\Messaging\MessageFactory::class,
                'plugins' => []
            ]
        ],

        'bus_router' => [

            'command_bus' => [
                'router' => [
                    'routes' => [],
                ],
            ],

            'event_bus' => [
                'router' => [
                    'routes' => [],
                ],
            ],

            'query_bus' => [
                'router' => [
                    'routes' => [],
                ],
            ],
        ]
    ]
];
